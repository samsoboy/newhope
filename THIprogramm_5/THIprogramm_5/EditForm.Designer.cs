﻿namespace THIprogramm_5
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancelbutton = new System.Windows.Forms.Button();
            this.OKbutton = new System.Windows.Forms.Button();
            this.DamagetextBox = new System.Windows.Forms.TextBox();
            this.DamageEdittextBox = new System.Windows.Forms.Label();
            this.WeighttextBox = new System.Windows.Forms.TextBox();
            this.WeightEdittextBox = new System.Windows.Forms.Label();
            this.PricetextBox = new System.Windows.Forms.TextBox();
            this.PriceEdittextBox = new System.Windows.Forms.Label();
            this.NametextBox = new System.Windows.Forms.TextBox();
            this.NameEdittextBox = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.CalibreEdittextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DurableReloadEdittextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RapidityEdittextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LengthEdittextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CritchanceEdittextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BleedchanseEdittextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PenetrationchanceEdittextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Cancelbutton
            // 
            this.Cancelbutton.Location = new System.Drawing.Point(191, 491);
            this.Cancelbutton.Name = "Cancelbutton";
            this.Cancelbutton.Size = new System.Drawing.Size(75, 23);
            this.Cancelbutton.TabIndex = 3;
            this.Cancelbutton.Text = "Cancel";
            this.Cancelbutton.UseVisualStyleBackColor = true;
            this.Cancelbutton.Click += new System.EventHandler(this.Cancelbutton_Click);
            // 
            // OKbutton
            // 
            this.OKbutton.Location = new System.Drawing.Point(110, 491);
            this.OKbutton.Name = "OKbutton";
            this.OKbutton.Size = new System.Drawing.Size(75, 23);
            this.OKbutton.TabIndex = 2;
            this.OKbutton.Text = "OK";
            this.OKbutton.UseVisualStyleBackColor = true;
            this.OKbutton.Click += new System.EventHandler(this.OKbutton_Click);
            // 
            // DamagetextBox
            // 
            this.DamagetextBox.Location = new System.Drawing.Point(12, 153);
            this.DamagetextBox.Name = "DamagetextBox";
            this.DamagetextBox.Size = new System.Drawing.Size(256, 20);
            this.DamagetextBox.TabIndex = 19;
            // 
            // DamageEdittextBox
            // 
            this.DamageEdittextBox.AutoSize = true;
            this.DamageEdittextBox.Location = new System.Drawing.Point(12, 136);
            this.DamageEdittextBox.Name = "DamageEdittextBox";
            this.DamageEdittextBox.Size = new System.Drawing.Size(47, 13);
            this.DamageEdittextBox.TabIndex = 18;
            this.DamageEdittextBox.Text = "Damage";
            // 
            // WeighttextBox
            // 
            this.WeighttextBox.Location = new System.Drawing.Point(12, 109);
            this.WeighttextBox.Name = "WeighttextBox";
            this.WeighttextBox.Size = new System.Drawing.Size(256, 20);
            this.WeighttextBox.TabIndex = 17;
            // 
            // WeightEdittextBox
            // 
            this.WeightEdittextBox.AutoSize = true;
            this.WeightEdittextBox.Location = new System.Drawing.Point(12, 92);
            this.WeightEdittextBox.Name = "WeightEdittextBox";
            this.WeightEdittextBox.Size = new System.Drawing.Size(41, 13);
            this.WeightEdittextBox.TabIndex = 16;
            this.WeightEdittextBox.Text = "Weight";
            // 
            // PricetextBox
            // 
            this.PricetextBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.PricetextBox.Location = new System.Drawing.Point(12, 69);
            this.PricetextBox.Name = "PricetextBox";
            this.PricetextBox.Size = new System.Drawing.Size(256, 20);
            this.PricetextBox.TabIndex = 15;
            // 
            // PriceEdittextBox
            // 
            this.PriceEdittextBox.AutoSize = true;
            this.PriceEdittextBox.Location = new System.Drawing.Point(12, 53);
            this.PriceEdittextBox.Name = "PriceEdittextBox";
            this.PriceEdittextBox.Size = new System.Drawing.Size(31, 13);
            this.PriceEdittextBox.TabIndex = 14;
            this.PriceEdittextBox.Text = "Price";
            // 
            // NametextBox
            // 
            this.NametextBox.Location = new System.Drawing.Point(12, 26);
            this.NametextBox.Name = "NametextBox";
            this.NametextBox.Size = new System.Drawing.Size(256, 20);
            this.NametextBox.TabIndex = 13;
            // 
            // NameEdittextBox
            // 
            this.NameEdittextBox.AutoSize = true;
            this.NameEdittextBox.Location = new System.Drawing.Point(12, 9);
            this.NameEdittextBox.Name = "NameEdittextBox";
            this.NameEdittextBox.Size = new System.Drawing.Size(35, 13);
            this.NameEdittextBox.TabIndex = 12;
            this.NameEdittextBox.Text = "Name";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(12, 176);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(39, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "Calibre";
            // 
            // CalibreEdittextBox
            // 
            this.CalibreEdittextBox.Location = new System.Drawing.Point(12, 193);
            this.CalibreEdittextBox.Name = "CalibreEdittextBox";
            this.CalibreEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.CalibreEdittextBox.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "DurableReload";
            // 
            // DurableReloadEdittextBox
            // 
            this.DurableReloadEdittextBox.Location = new System.Drawing.Point(12, 232);
            this.DurableReloadEdittextBox.Name = "DurableReloadEdittextBox";
            this.DurableReloadEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.DurableReloadEdittextBox.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 260);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Rapidity";
            // 
            // RapidityEdittextBox
            // 
            this.RapidityEdittextBox.Location = new System.Drawing.Point(12, 276);
            this.RapidityEdittextBox.Name = "RapidityEdittextBox";
            this.RapidityEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.RapidityEdittextBox.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 299);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Length";
            // 
            // LengthEdittextBox
            // 
            this.LengthEdittextBox.Location = new System.Drawing.Point(12, 320);
            this.LengthEdittextBox.Name = "LengthEdittextBox";
            this.LengthEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.LengthEdittextBox.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Crit Chance";
            // 
            // CritchanceEdittextBox
            // 
            this.CritchanceEdittextBox.Location = new System.Drawing.Point(12, 363);
            this.CritchanceEdittextBox.Name = "CritchanceEdittextBox";
            this.CritchanceEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.CritchanceEdittextBox.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 390);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Bleed chance";
            // 
            // BleedchanseEdittextBox
            // 
            this.BleedchanseEdittextBox.Location = new System.Drawing.Point(12, 407);
            this.BleedchanseEdittextBox.Name = "BleedchanseEdittextBox";
            this.BleedchanseEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.BleedchanseEdittextBox.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 430);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Penetration chance";
            // 
            // PenetrationchanceEdittextBox
            // 
            this.PenetrationchanceEdittextBox.Location = new System.Drawing.Point(12, 447);
            this.PenetrationchanceEdittextBox.Name = "PenetrationchanceEdittextBox";
            this.PenetrationchanceEdittextBox.Size = new System.Drawing.Size(256, 20);
            this.PenetrationchanceEdittextBox.TabIndex = 33;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 530);
            this.Controls.Add(this.PenetrationchanceEdittextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BleedchanseEdittextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CritchanceEdittextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LengthEdittextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RapidityEdittextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DurableReloadEdittextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CalibreEdittextBox);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.DamagetextBox);
            this.Controls.Add(this.DamageEdittextBox);
            this.Controls.Add(this.WeighttextBox);
            this.Controls.Add(this.WeightEdittextBox);
            this.Controls.Add(this.PricetextBox);
            this.Controls.Add(this.PriceEdittextBox);
            this.Controls.Add(this.NametextBox);
            this.Controls.Add(this.NameEdittextBox);
            this.Controls.Add(this.Cancelbutton);
            this.Controls.Add(this.OKbutton);
            this.Name = "EditForm";
            this.Text = "EditForm";
            this.Load += new System.EventHandler(this.EditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancelbutton;
        private System.Windows.Forms.Button OKbutton;
        private System.Windows.Forms.TextBox DamagetextBox;
        private System.Windows.Forms.Label DamageEdittextBox;
        private System.Windows.Forms.TextBox WeighttextBox;
        private System.Windows.Forms.Label WeightEdittextBox;
        private System.Windows.Forms.TextBox PricetextBox;
        private System.Windows.Forms.Label PriceEdittextBox;
        private System.Windows.Forms.TextBox NametextBox;
        private System.Windows.Forms.Label NameEdittextBox;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox CalibreEdittextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DurableReloadEdittextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RapidityEdittextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LengthEdittextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CritchanceEdittextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BleedchanseEdittextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PenetrationchanceEdittextBox;
    }
}