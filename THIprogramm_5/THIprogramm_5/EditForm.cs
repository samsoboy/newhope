﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THIprogramm_3;
using THIprogramm_4logic;

namespace THIprogramm_5
{
    public partial class EditForm : Form
    {
        private MainForm form1;
        public MachineGun _mg = new MachineGun();
        public Shotgun _sg = new Shotgun();
        public Edged _e = new Edged();
        public Trauma _t = new Trauma();
        public Pointed _p = new Pointed();
        public EditForm(MainForm form)
        {
            InitializeComponent();
            form1 = form;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _mg.GetType())
            {
                DurableReloadEdittextBox.Visible = false;
                LengthEdittextBox.Visible = false;
                CritchanceEdittextBox.Visible = false;
                BleedchanseEdittextBox.Visible = false;
                PenetrationchanceEdittextBox.Visible = false;
            } else if(form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _sg.GetType()) {
                RapidityEdittextBox.Visible = false;
                LengthEdittextBox.Visible = false;
                CritchanceEdittextBox.Visible = false;
                BleedchanseEdittextBox.Visible = false;
                PenetrationchanceEdittextBox.Visible = false;
            } else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _e.GetType()) {
                CalibreEdittextBox.Visible = false;
                DurableReloadEdittextBox.Visible = false;
                RapidityEdittextBox.Visible = false;
                CritchanceEdittextBox.Visible = false;
                PenetrationchanceEdittextBox.Visible = false;
            } else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _t.GetType()) {
                CalibreEdittextBox.Visible = false;
                DurableReloadEdittextBox.Visible = false;
                RapidityEdittextBox.Visible = false;
                CritchanceEdittextBox.Visible = false;
            } else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _p.GetType()) {
                CalibreEdittextBox.Visible = false;
                DurableReloadEdittextBox.Visible = false;
                RapidityEdittextBox.Visible = false;
                PenetrationchanceEdittextBox.Visible = false;
            }
        }

        private void OKbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _mg.GetType())
                {
                    MachineGun weapon = new MachineGun();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    weapon.Calibre = double.Parse(CalibreEdittextBox.Text);
                    weapon.Rapidity = int.Parse(RapidityEdittextBox.Text);
                    form1.mainDivision.WeaponList.RemoveAt(form1.WeaponListBox.SelectedIndex);
                    form1.WeaponListBox.Items.Remove(form1.WeaponListBox.SelectedItem);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _sg.GetType())
                {
                    Shotgun weapon = new Shotgun();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    weapon.Calibre = double.Parse(CalibreEdittextBox.Text);
                    weapon.DurableReload = double.Parse(DurableReloadEdittextBox.Text);
                    form1.mainDivision.WeaponList.RemoveAt(form1.WeaponListBox.SelectedIndex);
                    form1.WeaponListBox.Items.Remove(form1.WeaponListBox.SelectedItem);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _e.GetType())
                {
                    Edged weapon = new Edged();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    weapon.Length = double.Parse(LengthEdittextBox.Text);
                    weapon.Bleedchance = double.Parse(BleedchanseEdittextBox.Text);
                    form1.mainDivision.WeaponList.RemoveAt(form1.WeaponListBox.SelectedIndex);
                    form1.WeaponListBox.Items.Remove(form1.WeaponListBox.SelectedItem);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _t.GetType())
                {
                    Trauma weapon = new Trauma();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    weapon.Length = double.Parse(LengthEdittextBox.Text);
                    weapon.Critchance = double.Parse(CritchanceEdittextBox.Text);
                    form1.mainDivision.WeaponList.RemoveAt(form1.WeaponListBox.SelectedIndex);
                    form1.WeaponListBox.Items.Remove(form1.WeaponListBox.SelectedItem);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
                else if (form1.mainDivision.WeaponList[form1.WeaponListBox.SelectedIndex].GetType() == _p.GetType())
                {
                    Pointed weapon = new Pointed();
                    weapon.Name = NametextBox.Text;
                    weapon.Price = int.Parse(PricetextBox.Text);
                    weapon.Weigt = double.Parse(WeighttextBox.Text);
                    weapon.Damage = int.Parse(DamagetextBox.Text);
                    weapon.Length = double.Parse(LengthEdittextBox.Text);
                    weapon.Penetrarionchance = double.Parse(PenetrationchanceEdittextBox.Text);
                    form1.mainDivision.WeaponList.RemoveAt(form1.WeaponListBox.SelectedIndex);
                    form1.WeaponListBox.Items.Remove(form1.WeaponListBox.SelectedItem);
                    form1.mainDivision.WeaponList.Add(weapon);
                    form1.WeaponListBox.Items.Add(weapon.Name + " - " + weapon.Price);
                }
            }
            catch (Exception) { }
            this.Dispose();
        }

        private void Cancelbutton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        
    }
    
}
